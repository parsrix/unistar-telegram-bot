<?php
//++++++++++++++++++++++++++++++++++++++++//
// webhook settings: https://api.telegram.org/bot{bot_token}/setWebhook?url={url}
//++++++++++++++++++++++++++++++++++++++++//
include (__DIR__ . '/vendor/autoload.php');
require_once (__DIR__ . '/utils/jdf.php');
require_once (__DIR__ . '/functions.php');
require_once (__DIR__ . '/config.php');
#===================== Main =========================#
get_update();
initialize();

#--------------------- admin commands ---------------------#
if($user_text == 'نمایش آمار ربات' && is_admin()){
    show_bot_statistics();
} else if($user_text == 'جستجو براساس کد اشتراک' && is_admin()){
    $response_content['text'] = PHP_EOL.'کد اشتراک با پیشوند PR وارد نمایید.';
    send_message();
} else if(preg_match('/PR\d{'.$subscribe_code_width.'}/i', htmlspecialchars($user_text)) > 0 && is_admin()){
    get_subscribe_code_info();
}
#--------------------- admin commands ---------------------#
# 
#--------------------- client commands ---------------------#
else if ($user_text == '/start') {
    $response_content['text'] = 'به ربات خوش آمدید';
    send_message();
    $response_content['text'] = PHP_EOL . 'یک گزینه را انتخاب کنید';
    send_message();
} else if ($user_text == 'راهنمای استفاده') {
    $response_content['text'] .= PHP_EOL . '';
    send_message();
} else if ($user_text == 'پشتیبانی') {
    $response_content['text'] .= PHP_EOL . '';
    send_message();
} else if(!$user_registered) {
    user_register();
} else if(!$user_activated) {
    if(preg_match('/renew_grade_[0-9]/', $callback_data)){
        answer_callback();
        $grade = str_replace('renew_grade_', '', $callback_data);
        ready_payment_subscribe($grade);
    }
    else{
        renew_user_subscribe();
    }
} else{
    if($user_text == 'اشتراک گذاری' && $user_activated){
      share_post_user_keyboard();
      $response_content['text'] = PHP_EOL.'لطفا نوع اشتراک گذاری را مشخص نمایید';
      
    } else if($user_text == 'اشتراک پست' && $user_activated) {
        share_post_user_keyboard();
        $response_content['text'] = PHP_EOL.'پست خود را ارسال نمایید';
    } else if($user_text == 'اشتراک مقاله') {
        share_post_user_keyboard();
        $response_content['text'] = PHP_EOL.'مقاله مورد نظر را ارسال نمایید';
    } else if($user_text == 'اشتراک مقاله شخصی') {
        share_post_user_keyboard();
        $response_content['text'] = 'مقاله خود را ارسال نمایید';
    } else if($user_text == 'بازگشت به منوی اصلی') {
        initialize_user_keyboard();
        $response_content['text'] = PHP_EOL . 'یک گزینه را انتخاب کنید';
    } else {
        $response_content['text'] = 'درخواست شما نامعتبر می ';
    }
    send_message();
}
#--------------------- client commands ---------------------#

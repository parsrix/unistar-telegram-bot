<?php
require_once (__DIR__ . '/config.php');
#===================== General Functions ====================#
function logger($message){
    $logger_file_path = dirname(__FILEــ).'/log.txt';
    $myfile = fopen($logger_file_path, "a") or die("Unable to open file!");
    date_default_timezone_set("Asia/Tehran");
    $now = date("Y-m-d h:i:sa");
    fwrite($myfile, "\n".$now."\t".$message);
    fclose($myfile);
}
function check_user_registeration_and_activation(){
    global $register_level, $dbconn, $chat_id, $user_registered, $user_activated;
    $redis_data = get_redis_data();
    if($redis_data){
        if(isset($redis_data['register_level'])){
            $register_level = intval($redis_data['register_level']);
        }
        if(isset($redis_data['is_active'])){
            $is_active = intval($redis_data['is_active']);
        }
    } else {
        $query = mysqli_query($dbconn, "SELECT register_level, is_active FROM "
                . "`users` WHERE `user_id` = '$chat_id' LIMIT 1");
        if (mysqli_num_rows($query) > 0) {
            $row = mysqli_fetch_array($query);
            $register_level = $row['register_level'];
            $is_active = $row['is_active'];
        }
    }
    if($register_level == 8){
        $user_registered = TRUE;
    }
    if($is_active == 1){
        $user_activated = TRUE;
    }
}
function user_register(){
    global $chat_id, $register_level, $response_content, $dbconn, $user_text;
    global $user_registered, $user_activated, $telegram, $keyboard;
    global $redis_force_usage;
    if($user_registered){
        if($user_activated){
            $response_content['text'] = PHP_EOL.'شما قبلا عضو شده اید'; 
        }
        else{
            $response_content['text'] = PHP.EOL.'حساب کاربری شما فعال نیست'
                . 'با پشتیبانی در تماس باشید';
        }
    } else{
//+++++++++++++++++++++++++++++++++++++++++//
// register_level < 0 => prompt for <first name>
// register_level == 0 => save <first name> and prompt for <last name>
// register_level == 1 => save <last name> and prompt for <phone number>
// register_level == 2 => save <phone number> and prompt for get <nationality id>
// register_level == 3 => save <nationality id> and prompt for <birth day>
// register_level == 4 => save <birth day> and prompt for <student id>
// register_level == 5 => save <student id> and prompt for <city>
// register_level == 6 => save <city> and prompt for <university>
// register_level == 7 => save <university> and prompt to successfully registered
//+++++++++++++++++++++++++++++++++++++++++//
        if($register_level < 0 || $user_text == 'عضویت' || 
                $user_text == '/register'){
            // prompt for <first name>
            if(!$redis_force_usage){
                mysqli_query($dbconn, "DELETE FROM `users` WHERE `user_id` = "
                        . "'$chat_id'"); 
                
                mysqli_query($dbconn, "INSERT INTO `users` (user_id, "
                        . "register_level) VALUES ('$chat_id', '0')"); 
            } else {
                remove_redis_data();
                remove_redis_data('_register');
            }
            $register_level = 0;
            $response_content['text'] = PHP_EOL . 'نام خود را وارد کنید';
        } else if($register_level == 0){
            // save <first name> and prompt for <last name>
            $safe_user_text = htmlspecialchars($user_text);
            if (preg_match('/(.)*(\d)+(.)*/', $safe_user_text) <= 0) {
                if(!$redis_force_usage){
                    mysqli_query($dbconn, "UPDATE `users` SET `first_name` = '"
                            . "$safe_user_text', `register_level` = '1' WHERE "
                            . "`user_id` = '$chat_id'");
                } else {
                    $cache_updated_fields = array(
                        'first_name' => $safe_user_text
                    );
                    update_redis_register_data($cache_updated_fields);
                }
                $register_level = 1;
                $response_content['text'] = PHP_EOL . 
                        'نام خانوادگی خود را وارد کنید';
            } else {
                $response_content['text'] = PHP_EOL .
                        'نام خود را به درستی وارد کنید';
            }
        } else if($register_level == 1){
            // save <last name> and prompt for <phone number>
            $safe_user_text = htmlspecialchars($user_text);
            if (preg_match('/(.)*(\d)+(.)*/', $safe_user_text) <= 0) {
                if(!$redis_force_usage){
                    mysqli_query($dbconn, "UPDATE `users` SET `last_name` = '"
                            . "$safe_user_text', `register_level` = '2' WHERE "
                            . "`user_id` = '$chat_id'");
                } else {
                    $cache_updated_fields = array(
                        'last_name' => $safe_user_text
                    );
                    update_redis_register_data($cache_updated_fields);
                }
                $register_level = 2;
                $response_content['text'] = PHP_EOL .
                        'تلفن همراه خود را وارد کنید';
            } else {
                $response_content['text'] = PHP_EOL . 
                        'نام خانوادگی خود را به درستی وارد کنید';
            }
        } else if($register_level == 2){
            // save <phone number> and prompt for get <nationality id>
            if (preg_match('/^09([0-9]){9}$/', $user_text) > 0 || preg_match('/^9([0-9]){10}$/', $user_text) > 0) {
                $safe_user_text = intval(htmlspecialchars($user_text));
                if(!$redis_force_usage){
                    mysqli_query($dbconn, "UPDATE `users` SET `phone_number` = "
                            . "'$safe_user_text', `register_level` = '3' WHERE "
                            . "`user_id` = '$chat_id'");
                } else {
                    $cache_updated_fields = array(
                        'phone_number' => $safe_user_text
                    );
                    update_redis_register_data($cache_updated_fields);
                }
                $register_level = 3;
                $response_content['text'] = PHP_EOL .
                        'کدملی خود را وارد نمایید';
            } else {
                $response_content['text'] = PHP_EOL . 
                        'تلفن همراه خود را به درستی وارد کنید';
            }
        } else if($register_level == 3){
            // save <nationality id> and prompt for <birth day>
            if (preg_match('/[1-9]([0-9]){9}/', $user_text) > 0){
                $safe_user_text = intval(htmlspecialchars($user_text));
                if(!$redis_force_usage){
                    mysqli_query($dbconn, "UPDATE `users` SET `nationality_id` = "
                            . "'$safe_user_text', `register_level` = '4' WHERE "
                            . "`user_id` = '$chat_id'");
                } else {
                    $cache_updated_fields = array(
                        'nationality_id' => $safe_user_text
                    );
                    update_redis_register_data($cache_updated_fields);
                }
                $register_level = 4;
                $response_content['text'] = PHP_EOL .
                        'تاریخ تولد خود را وارد نمایید'.PHP_EOL.
                        'مثال: 1370/01/02';
            } else {
                $response_content['text'] = PHP_EOL . 
                        'کدملی خود را به درستی وارد کنید';
            }
            
        } else if($register_level == 4){
            // save <birth day> and prompt for <student id>
            $date = explode('/', htmlspecialchars($user_text));
            if (preg_match('/13([0-9]){2}/', $date[0]) > 0 && intval($date[1]) && intval($date[2])){
                $georgian_date = jalali_to_gregorian($date[0], $date[1], $date[2], '-');
                if(!$redis_force_usage){
                    mysqli_query($dbconn, "UPDATE `users` SET `birthday` = "
                            . "'$georgian_date', `register_level` = '5' WHERE "
                            . "`user_id` = '$chat_id'");
                } else {
                    $cache_updated_fields = array(
                        'birthday' => $georgian_date
                    );
                    update_redis_register_data($cache_updated_fields);
                }
                $register_level = 5;
                $response_content['text'] = PHP_EOL .
                        'شماره دانشجویی خود را وارد نمایید';
            } else {
                $response_content['text'] = PHP_EOL .
                        'تاریخ تولد خود را به درستی وارد نمایید';
            }
        } else if($register_level == 5){
            // save <student id> and prompt for <city>
            $safe_user_text = intval(htmlspecialchars($user_text));
            if (preg_match('/[1-9]([0-9])+/', $safe_user_text) > 0) {
                if(!$redis_force_usage){
                    mysqli_query($dbconn, "UPDATE `users` SET `student_id` = '"
                            . "$safe_user_text', `register_level` = '6' WHERE "
                            . "`user_id` = '$chat_id'");
                } else {
                    $cache_updated_fields = array(
                        'student_id' => $safe_user_text
                    );
                    update_redis_register_data($cache_updated_fields);
                }
                $register_level = 6;
                $response_content['text'] = PHP_EOL .
                        'شهر محل تحصیل خود را وارد نمایید';
            } else {
                $response_content['text'] = PHP_EOL . 
                        'شماره دانشجویی خود را به درستی وارد نمایید';
            }
        } else if($register_level == 6){
            // save <city> and prompt for <university>
            $safe_user_text = htmlspecialchars($user_text);
            if (preg_match('/(.)*(\d)+(.)*/', $safe_user_text) <= 0) {
                if(!$redis_force_usage){
                    mysqli_query($dbconn, "UPDATE `users` SET `city` = '"
                            . "$safe_user_text', `register_level` = '7' WHERE "
                            . "`user_id` = '$chat_id'");
                } else {
                    $cache_updated_fields = array(
                        'city' => $safe_user_text
                    );
                    update_redis_register_data($cache_updated_fields);
                }
                $register_level = 7;
                $response_content['text'] = PHP_EOL . 
                        'نام دانشگاه خود را وارد نمایید';
            } else {
                $response_content['text'] = PHP_EOL .
                        'شهر محل تحصیل خود را به درستی وارد نمایید';
            }
        } else if($register_level == 7){
            // save <university> and prompt to successfully registered
            $safe_user_text = htmlspecialchars($user_text);
            if (preg_match('/(.)*(\d)+(.)*/', $safe_user_text) <= 0) {
                $register_level = 8;
                $user_registered = TRUE;
                $user_activated = FALSE;
                if($redis_force_usage){
                    $redis_data = get_redis_data('_register');
                    mysqli_query($dbconn, "INSERT INTO `users` "
                            . "(`user_id`, `is_active`, `first_name`, "
                            . "`last_name`, `phone_number`, `birthday`,"
                            . "`nationality_id`, `student_id`, `city`,"
                            . "`university`, `register_level`) "
                            . "VALUES ("
                            . "'$chat_id', '"
                            . "0', '"
                            .$redis_data['first_name']."', '"
                            .$redis_data['last_name']."', '"
                            .$redis_data['phone_number']."', '"
                            .$redis_data['birthday']."', '"
                            .$redis_data['nationality_id']."', '"
                            .$redis_data['student_id']."', '"
                            .$redis_data['city']."', '"
                            .$safe_user_text."', '"
                            .$register_level."')");
                    remove_redis_data('_register');
                } else {
                    mysqli_query($dbconn, "UPDATE `users` SET `university` = '"
                            . "$safe_user_text', `register_level` = "
                            . "'$register_level', "
                            . "`is_active` = 0 WHERE `user_id` = '$chat_id'");
                }
                $response_content['text'] = PHP_EOL . 
                        'عضویت شما با موفقیت انجام شد';
                $keyboard[0][2] = $telegram->buildKeyboardButton("اشتراک گذاری");
            } else {
                $response_content['text'] = PHP_EOL .
                        'نام دانشگاه خود را به درستی وارد نمایید';
            }
        }
    }
    update_redis_data(array('register_level' => $register_level));
    send_message();
    if($user_registered && !$user_activated){
        $response_content['text'] = 'شما باید اشتراک خود را فعال کنید';
        send_message();
        renew_user_subscribe();
        # TODO: consider update user info in redis cache when user get activated
    }
}
function initialize_user_keyboard(){
    global $telegram, $keyboard, $user_registered, $response_content;
    // general keyboard
    $keyboard = array(
        array($telegram->buildKeyboardButton("پشتیبانی"),
            $telegram->buildKeyboardButton("راهنمای استفاده"),
        )
    );
    if(! $user_registered){
        $keyboard[0][2] = $telegram->buildKeyboardButton("عضویت");
    } else {
        $keyboard[0][2] = $telegram->buildKeyboardButton("اشتراک گذاری");
    }
    if(is_admin()){
        $keyboard[1] = array(
            $telegram->buildKeyboardButton("جستجو براساس کد اشتراک"),
        //$telegram->buildKeyboardButton("جستجو براساس شماره موبایل"),
            $telegram->buildKeyboardButton("نمایش آمار ربات")
                );
    }
    $response_content['reply_markup'] = $telegram->buildKeyBoard($keyboard, 
        false, true);
}
function is_admin(){
    global $dbconn, $chat_id, $redis;
    $admins = false;
    if(redis_connection_test()){
        try{
            $admins = $redis->get('admins');
        } catch (Exception $e) {
            exception_logger("Exception occured when try to get admins from "
                    . "redis".PHP_EOL."Exception: ".$e->getMessage()); 
        }
    }
    // admins section
    if(!$admins){
        $query = mysqli_query($dbconn, "SELECT * FROM `settings` WHERE `name`"
                . " = 'admins' LIMIT 1");
        if (mysqli_num_rows($query) > 0) {
            $row = mysqli_fetch_array($query);
            $admins = $row['value'];
            if(redis_connection_test()){
                try{
                    $redis->set('admins', $admins);
                } catch (Exception $e) {
                    exception_logger("Exception occured when try to get admins from redis"
                            .PHP_EOL."Exception: ".$e->getMessage()); 
                }
            }
        }
    }
    return (in_array($chat_id, explode(',', $admins)));
}
function get_update(){
    global $telegram, $user_text, $chat_id, $response_content, $callback_data;
    global $callback_query_id;
    // get user request
    $user_text = $telegram->Text();
    $chat_id = $telegram->ChatID();
    $callback_data = $telegram->Callback_Query()['data'];
    $callback_query_id = $telegram->Callback_ID();
    $response_content = array('chat_id' => $chat_id);
}
function show_bot_statistics(){
    global $dbconn, $response_content;
    $query = mysqli_query($dbconn, 
            "SELECT COUNT(*) as user_count FROM `users`");
    if (mysqli_num_rows($query) > 0) {
        $row = mysqli_fetch_array($query);
        $user_count = $row['user_count'];
    }
    $response_content['text'] = PHP_EOL.'تعداد کاربران: '.$user_count;
    send_message();
}
function send_message(){
    global $telegram, $keyboard, $response_content, $keyboard_inline_mode;
    if($keyboard_inline_mode){
        $response_content['reply_markup']  = $telegram->buildInlineKeyBoard(
            $keyboard, false, true);
    } else {
        $response_content['reply_markup'] = $telegram->buildKeyBoard($keyboard, 
            false, true);
    }
    if(isset($response_content['text']) && !empty($response_content['text'])){
        $telegram->sendMessage($response_content);
    }
}
function get_subscribe_code_info(){
    global $user_text, $dbconn, $response_content;
    $safe_user_text = preg_replace('/PR/i', '', htmlspecialchars($user_text));
    $query = mysqli_query($dbconn, "SELECT * FROM `users` WHERE `subscribe_code`"
            . "='$safe_user_text' LIMIT 1");
    if (mysqli_num_rows($query) > 0) {
        $row = mysqli_fetch_array($query);
        $response_content['text'] = PHP_EOL.'نام: '.$row['first_name'];
        $response_content['text'] .= PHP_EOL.'نام خانوادگی: '.$row['last_name'];
        $response_content['text'] .= PHP_EOL.'تلفن همراه: '.$row['phone_number'];
        $response_content['text'] .= PHP_EOL.'کد اشتراک: '.$row['subscribe_code'];
    }
    send_message();
}
function initialize(){
    global $response_content, $chat_id;
    $response_content = array('chat_id' => $chat_id);
    check_user_registeration_and_activation();
    initialize_user_keyboard();
    if(is_redis_db_empty()){
        reset_redis_cache();
    }
}
function share_post_user_keyboard(){
    global $telegram, $keyboard, $response_content;
    // general keyboard
    $keyboard = array(
        array($telegram->buildKeyboardButton("اشتراک مقاله شخصی"),
            $telegram->buildKeyboardButton("اشتراک مقاله"),
            $telegram->buildKeyboardButton("اشتراک پست"),
            ),
        array($telegram->buildKeyboardButton("راهنمای استفاده"),
            $telegram->buildKeyboardButton("بازگشت به منوی اصلی")
            )
    );
    $response_content['reply_markup'] = $telegram->buildKeyBoard($keyboard, 
        false, true);
}
function renew_user_subscribe(){
    global $keyboard_inline_mode, $keyboard, $telegram, $response_content;
    global $dbconn, $chat_id;
    $query = mysqli_query($dbconn, "SELECT * FROM `subscribe`");
    if (mysqli_num_rows($query) <= 0) {
        exception_logger("problem in get data from subscribe database "
                . "table", TRUE);
    }
    else {
        $keyboard_inline_mode = TRUE;
        $keyboard = array();
        $i = -1;
        $j = 0;
        while($row = mysqli_fetch_array($query)){
            if($j % 2 == 0){
                $i++;
                $j = 0;
                $keyboard[$i] = array();
            }
            $keyboard[$i][$j] = $telegram->buildInlineKeyBoardButton(
                    $row['name'], "", "renew_grade_".$row['id']);
            $j++;
        }
        $response_content['text'] = 'مقطع تحصیلی خود را انتخاب کنید';
    }
    send_message();
}
function answer_callback(){
    global $telegram, $keyboard, $response_content, $callback_query_id;
    if(!empty($callback_query_id)){
        $response_content['callback_query_id'] = $callback_query_id;
    }
    $response_content['reply_markup']  = $telegram->buildInlineKeyBoard(
            $keyboard, false, true);
    $telegram->answerCallbackQuery($response_content);
}
function exception_logger($bug_message, $exit = FALSE){
    global $chat_id, $response_content;
    logger("chat_id: $chat_id, $bug_message");
    $response_content['text'] = PHP_EOL.'اشکالی در سامانه وجود دارد'.PHP_EOL
                .'لطفا با پشتیبانی در ارتباط باشید';
    send_message();
    if($exit){
        exit();
    }
}
#===================== General Functions ====================#
#
#===================== Payment Functions ====================#
function ready_payment_subscribe($grade){
    global $chat_id, $dbconn;
    if($grade >= 1 && $grade <= 4){
        # TODO: apply redis
        $query = mysqli_query($dbconn, "SELECT name, amount FROM `subscribe` "
                . "WHERE `id` = $grade LIMIT 1");
        if (mysqli_num_rows($query) > 0) {
            $row = mysqli_fetch_array($query);
            $Amount = $row['amount'];
            $Description = 'تمدید اشتراک برای مقطع '.$row['name'];
        }
        if(redis_connection_test()){
            $redis_data = get_redis_data();
            if($redis_data && $redis_data['phone_number'] != 'null'){
                $Mobile = $redis_data['phone_number'];
            }
        }
        if(!isset($Mobile)){
            $query = mysqli_query($dbconn, "SELECT phone_number FROM `users` "
                    . "WHERE `user_id` = $chat_id LIMIT 1");
            if (mysqli_num_rows($query) > 0) {
                $row = mysqli_fetch_array($query);
                if(redis_connection_test()){
                    $cache_updated_fields = array(
                        'phone_number' => $row['phone_number']
                            );
                    update_redis_data($cache_updated_fields);
                }
                $Mobile = $row['phone_number'];
            }
        }
        zarinpal_payment($Amount, $Description, $Mobile);
    } else{
        exception_logger("undefined grade in ready_payment_subscribe!");
    }
}
function zarinpal_payment($Amount, $Description, $Mobile){
    global $zarinpal_callback_url, $zarinpal_merchant_id, $response_content;
    global $keyboard, $telegram, $keyboard_inline_mode;
    $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl'
            , ['encoding' => 'UTF-8']);
    $result = $client->PaymentRequest(
            [ 'MerchantID' => $zarinpal_merchant_id, 'Amount' => $Amount,
                'Description' => $Description, 'Mobile' => $Mobile,
                'CallbackURL' => $zarinpal_callback_url,
                ]
            );
    if ($result->Status == 100) {
        # TODO: add or update zarinpal's amount and authority in redis cache
        $keyboard_inline_mode = TRUE;
        //---indirect---//
//        $keyboard = array(array($telegram->buildInlineKeyBoardButton(
//                    'انتقال به درگاه بانکی', 
//                'https://www.zarinpal.com/pg/StartPay/'.$result->Authority)));
        //---zaringate---//
        $keyboard = array(array($telegram->buildInlineKeyBoardButton(
                    'انتقال به درگاه بانکی', 'https://www.zarinpal.com/pg/'
                . 'StartPay/' .$result->Authority.'/ZarinGate')));
        $response_content['text'] = PHP_EOL."مبلغ قابل پرداخت: $Amount تومان"
            .PHP_EOL.'برای پرداخت، دکمه زیر را فشار دهید';
        send_message();
    } else {
        # TODO: remove zarinpal's amount, authority from redis cache
        exception_logger('ERR: '.$result->Status);
    }
}
function zarinpal_authenticate_transaction(){
    global $zarinpal_merchant_id, $response_content, $chat_id;
    # TODO: usage zarinpal_authenticate_transaction function as zarinpal callback
    # TODO: dynamic get amount from redis db
    $Amount = 1000; //Amount will be based on Toman
    $Authority = htmlspecialchars($_GET['Authority']);
    if (htmlspecialchars($_GET['Status']) == 'OK') {
        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/'
                . 'wsdl' , ['encoding' => 'UTF-8']);
        $result = $client->PaymentVerification(
                ['MerchantID' => $zarinpal_merchant_id, 
                    'Authority' => $Authority, 'Amount' => $Amount,
                    ]
                );
        # TODO: remove zarinpal's amount, authority from redis cache
        if ($result->Status == 100) {
            # TODO: activate user account in database
            $response_content['text'] = PHP_EOL.'پرداخت شما با موفقیت انجام شد'
                    .PHP_EOL.'شماره تراکنش شما '.$result->RefID.' می باشد';
            send_message();
            $response_content['text'] = PHP_EOL
                    .'جساب کاربری شما فعال شد'
                    .PHP_EOL.'حال می توانید از تمامی امکانات سامانه'
                    . ' استفاده نمایید';
            send_message();
        } else {
            # TODO: inactivate user account in database if needed
            logger("chat_id: $chat_id, Transaction failed. Status: "
            . "$result->Status");
            $response_content['text'] = 'تراکنش شما ناموفق بود';
            send_message();
        }
    } else {
        logger("chat_id: $chat_id, Transaction canceled by user");
            $response_content['text'] = 'تراکتش توسط شما کنسل شد';
            send_message();
    }
}
//function get_previous_update(){
//    global $telegram;
//    $telegram->UpdateID();
//    
//}
#===================== Redis and Caching Functions ====================#
function redis_connection_test(){
    global $redis, $redis_force_usage, $redis_hostname, $redis_port;
    try {
        if($redis->ping() == "+PONG"){
            return TRUE;
        }
    } catch (Exception $e) {
        try {
            $redis->connect($redis_hostname, $redis_port);
        } catch (Exception $e) {
            if($redis_force_usage){
            exception_logger("Exception occured when try to connect to "
                    . "redis service".PHP_EOL."Exception: ".$e->getMessage(),
                    $redis_force_usage);
            } else {
                logger("Exception occured when try to connect to redis service"
                        .PHP_EOL."Exception: ".$e->getMessage());
            }
        }
        if($redis->ping() == "+PONG"){
            return TRUE;
        } else {
            exception_logger("can't connect to redis service!", 
                    $redis_force_usage);
            return FALSE;
        }
    }
}
function reset_redis_cache($cache_fields_extra = array(), 
        $db_fields_extra = array()){
    global $redis, $dbconn;
    # TODO: remvoe redis_filled if not needed
    global $redis_filled;
    global $default_db_value_fields, $default_cache_value_fields;
    if(redis_connection_test()){
        $db_fields = array_merge($default_db_value_fields, $db_fields_extra);
        if(count($db_fields) <= 0){
            exception_logger('fields array length is invalid', TRUE);
        } 
        $redis->flushAll();
        $query = mysqli_query($dbconn, "SELECT ".implode(',', $db_fields)
                ." FROM `users`");
        # TODO: fix array_merge for associative arrays and fix in while
        $cache_fields = array_merge($default_cache_value_fields, 
                $cache_fields_extra);
        while($row = mysqli_fetch_array($query)){
            $value_list = array();
            foreach ($cache_fields as $cache_key => $cache_value){
                    $value_list[$cache_key] = isset($row[$cache_key]) ? 
                            $row[$cache_key] : 
                        $default_cache_value_fields[$cache_key];
            }
            $redis->set($row['user_id'], json_encode($value_list));
        }
        // add admins in redis
        $query = mysqli_query($dbconn, "SELECT * FROM `settings` WHERE `name` = "
            . "'admins' LIMIT 1");
        if (mysqli_num_rows($query) > 0) {
            $row = mysqli_fetch_array($query);
            $admins = explode(',', $row['value']);
        }   
        $redis->set('admins', json_encode($admins));
        $redis_filled = TRUE;
    } 
}
function is_redis_db_empty(){
    global $redis;
    if(redis_connection_test()){
        $keys = $redis->keys('*');
        if(count($keys) > 0){
            return FALSE;
        }
        return TRUE;
    }
    return FALSE;
}
function update_redis_data($cache_updated_fields){
    global $default_cache_value_fields, $redis, $chat_id;
    if(redis_connection_test()){
        $old_cache_fields = get_redis_data();
        if($old_cache_fields){
            $cache_fields = array_merge($old_cache_fields,
                $cache_updated_fields);
        } else {
            $cache_fields = array_merge($default_cache_value_fields,
                $cache_updated_fields);
        }
        $redis->set($chat_id, json_encode($cache_fields));
    } else{
        exception_logger('failed to insert or update redis cache');
    }
}
function update_redis_register_data($cache_updated_fields){
    global $default_cache_register_value_fields, $redis, $chat_id;
    $postfix = '_register';
    if(redis_connection_test()){
        $old_cache_fields = get_redis_data($postfix);
        if($old_cache_fields){
            $cache_fields = array_merge($old_cache_fields,
                $cache_updated_fields);
        } else {
            $cache_fields = array_merge($default_cache_register_value_fields,
                $cache_updated_fields);
        }
        $redis->set($chat_id.$postfix, json_encode($cache_fields));
    } else{
        exception_logger('failed to insert or update redis cache');
    }
}
function get_redis_data($postfix = ""){
    global $chat_id, $redis;
    if(redis_connection_test()){
        $redis_data = $redis->get($chat_id.$postfix);
        if($redis_data && $redis_data != 'null'){
            return json_decode($redis_data, true);
        }
        # TODO: consider 'else' for load data from main db to redis and try again 
    }
    return FALSE;
}
function remove_redis_data($postfix = ""){
    global $chat_id, $redis;
    if(redis_connection_test()){
        $redis->delete($chat_id.$postfix);
        return TRUE;
    }
    return FALSE;
}
#===================== Redis and Caching Functions ====================#

<?php
#===================== Settings =====================#
$bot_token = '';
$db_hostname = 'localhost';
$db_name = '';
$db_user = '';
$db_pass = '';
#===================== Settings =====================#
#
#===================== Payment =======================#
$zarinpal_merchant_id = 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'; //Required
$zarinpal_callback_url = 'https://domain.com/verify.php';
#===================== Payment =======================#
#
#===================== Global =======================#
$register_level = -1;
$user_registered = FALSE;
$user_activated = FALSE;
$dbconn = mysqli_connect($db_hostname, $db_user, $db_pass, $db_name) 
        or die(mysqli_error("Can not Established connect to database!"));
mysqli_set_charset($dbconn, 'utf8');
$telegram = new Telegram($bot_token);
$user_text = "";
$chat_id = "";
$response_content = array();
$callback_data = "";
$callback_query_id = "";
$keyboard = "";
$keyboard_inline_mode = FALSE;
#===================== Global =======================#
#
#===================== Redis =======================#
$redis_hostname = '127.0.0.1';
$redis_port = '6379';
$redis = new Redis();
$redis_filled = FALSE;
$redis_force_usage = TRUE;
$default_db_value_fields = array('user_id', 'is_active', 'score', 
    'register_level', 'phone_number');
$default_cache_value_fields = array(
    'is_active' => 0,
    'post_count' => 0,
    'general_paper_count' => 0,
    'self_paper_count' => 0,
    'score' => 0,
    'register_level' => -1,
    'phone_number' => 'null'
    );
$default_cache_register_value_fields = array(
    'first_name' => '',
    'last_name' => '',
    'phone_number' => '',
    'birthday' => '',
    'nationality_id' => '',
    'student_id' => '',
    'city' => '',
    'university' => '',
    );

#===================== Redis =======================#
#===================== Debug =======================#
#
#===================== Debug =======================#
